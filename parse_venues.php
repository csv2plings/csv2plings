<?php
# Copyright (c) 2009 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.

require_once "parse_defs.php";

function venues() {
    global $template_venues, $xmleq_venues, $locala, $fix;
    $spread = fopen("data/".$locala."_v.csv", "r");
    $header = fgetcsv($spread);
    checkhead($header, $template_venues);

    while ($row = fgetcsv($spread)) {
        if ($row[0] != "") {
        $row[0] = append_extra($row[0]);
        if (trimbadchar($row[6]) == "" && $row[7] == "") {
            if ($fix) {
                $row[6] = "n/a";
                berror("<del>Either a contact email or a contact phone must be specified</del> <i>Replaced with placeholder text</i>",$row[0]);
            }
            else {
                berror("Either a contact email or a contact phone must be specified",$row[0]);
            }
        }
        if ($row[4] == "" && $row[5] == "") {
            $row[4] = "General";
            $row[5] = "enquiries";
            berror("<del>Contact name not supplied</del> <i>Replaced with armyman</i>",$row[0]);
        }
        foreach ($row as $key => $field) {
            if ($field == "" && $key < 6) {
                if ($key == 4 || $key == 5 || $key == 2 | $key == 3) {
                    $field = "n/a";
                    berror("<del>Required field ".$xmleq_venues[$key]." is missing.</del> <i>Replaced with placeholder text</i>", $row[0]);
                }
                else {
                    berror("Required field ".$xmleq_venues[$key]." is missing.", $row[0]);
                }
            }
            $venues[$row[0]][$xmleq_venues[$key]] = $field;
        }
        }
    }
    return $venues;
}

function providers() {
    global $template_providers, $xmleq_providers, $locala, $fix;
    $spread = fopen("data/".$locala."_o.csv", "r");
    $header = fgetcsv($spread);
    checkhead($header, $template_providers);

    while ($row = fgetcsv($spread)) {
        if ($row[0] != "") {
        $row[0] = append_extra($row[0]);
        if ($locala == "islington") {
            $name = $row[9];
            $space = strpos($name, " ");
            if ($space) {
                $row[9] = substr($name, 0, $space);
                $row[10] = substr($name, $space+1, -1);
            }
        }
        if ($row[8] == "" && $row[11] == "" && $row[12] == "") {
            if ($fix) {
                $row[12] = "n/a";
                berror("<del>Either a postcode, contact email or a contact phone must be specified</del> <i>Replaced with placeholder text</i>",$row[0]);
            }
            else {
                berror("Either a postcode, contact email or a contact phone must be specified",$row[0]);
            }
        }
        if ($row[8] != "" && $row[3] == "") {
            if ($fix) {
                $row[3] = "n/a";
                berror("<del>No buildingnameno.</del> <i>Replaced with placeholder text.</i>");
            }
        }
        if ($row[9] == "" && $row[10] == "") {
            $row[9] = "General";
            $row[10] = "enquiries";
            berror("<del>Contact name not supplied</del> <i>Replaced with armyman</i>",$row[0]);
        }
        foreach ($row as $key => $field) {
            if ($field == "" && ($key == 0 || $key == 1 || $key == 9 || $key == 10)) {
                if ($key == 9 || $key == 10) {
                    $field = "n/a";
                    berror("<del>Required field ".$xmleq_providers[$key]." is missing.</del> <i>Replaced with placeholder text</i>", $row[0]);
                }
                else {
                    berror("Required field ".$xmleq_providers[$key]." is missing.", $row[0]);
                }
            }
            $providers[$row[0]][$xmleq_providers[$key]] = $field;
        }
        }
    }
    return $providers;
}

?>
