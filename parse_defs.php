<?php
# Copyright (c) 2009 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.

// These are INCORRECT (from islington, not the provided ones!)
$template = Array
(
    0 => "Activity ID",
    1 => "Name",
    2 => "Description",
    3 => "Start Date",
    4 => "End Date",
    5 => "Day",
    6 => "Starts",
    7 => "Ends",
    8 => "ContactName",
    9 => "ContactNumber",
    10 => "ContactEmail",
    11 => "ContactAddress",
    12 => "MinAge",
    13 => "MaxAge",
    14 => "Cost",
    15 => "Categories",
    16 => "Keywords",
    17 => "ECMCodes",
    18 => "Venue ID",
    19 => "Organisation ID"
);
$xmleq = Array
(
    0 => "ActivitySourceID",
    1 => "Name",
    2 => "Description",
    3 => "n/a",
    4 => "n/a",
    5 => "Day??",
    6 => "Starts",
    7 => "Ends",
    8 => "ContactName",
    9 => "ContactNumber",
    10 => "ContactEmail",
    11 => "ContactAddress",
    12 => "MinAge",
    13 => "MaxAge",
    14 => "Cost",
    15 => "Categories",
    16 => "Keywords",
    17 => "ECMCodes",
    18 => "Venue",
    19 => "ActivityProvider"
);
# Array(Database order, Field name, required)
$xmleq2 = Array
(
    Array(0, "ActivitySourceID", ),
    Array(1, "Name", true),
    Array(2, "Description", true),
    Array(6, "Starts", true),
    Array(7, "Ends", true),
    Array(18, "Venue", true),
    Array(19, "ActivityProvider", false),
    Array(8, "ContactName", true),
    Array(9, "ContactNumber",false),
    Array(10, "ContactEmail", false),
    Array(11, "ContactAddress", false),
    Array(12, "MinAge", false),
    Array(13, "MaxAge", false),
    Array(14, "Cost", false),
    Array(15, "Categories", false),
    Array(16, "Keywords", false),
    Array(17, "ECMCodes", false)
);
$xmleq_recurring = Array
(
    0 => "ActivitySourceID",
    1 => "Name",
    2 => "Description",
    3 => "n/a",
    4 => "n/a",
    5 => "Day??",
    6 => "n/a",
    7 => "Starts",
    8 => "Ends",
    9 => "ContactName",
    10 => "ContactNumber",
    11 => "ContactEmail",
    //12 => "ContactAddress",
    12 => "MinAge",
    13 => "MaxAge",
    14 => "Cost",
    15 => "Categories",
    16 => "Keywords",
    17 => "ECMCodes",
    18 => "Venue",
    19 => "ActivityProvider"
);
$xmleq2_recurring = Array
(
    Array(0, "ActivitySourceID", ),
    Array(1, "Name", true),
    Array(2, "Description", true),
    Array(7, "Starts", true),
    Array(8, "Ends", true),
    Array(19, "Venue", true),
    Array(20, "ActivityProvider", false),
    Array(9, "ContactName", true),
    Array(10, "ContactNumber",false),
    Array(11, "ContactEmail", false),
    Array(12, "ContactAddress", false),
    Array(13, "MinAge", false),
    Array(14, "MaxAge", false),
    Array(15, "Cost", false),
    Array(16, "Categories", false),
    Array(17, "Keywords", false),
    Array(18, "ECMCodes", false)
);


$template_venues = Array
(
    0 => "Venue ID",
    1 => "Name",
    2 => "BuildingNameNo",
    3 => "Postcode",
    4 => "ContactForename",
    5 => "ContactSurname",
    6 => "ContactTelephone",
    7 => "Contactemail",
    8 => "ContactFax",
    9 => "description",
    10 => "website",
    11 => "parking",
    12 => "CyclePark",
    13 => "DisabledFacilitiesNotes"
);
$xmleq_venues = Array
(
    0 => "ProviderVenueID",
    1 => "Name",
    2 => "BuildingNameNo",
    3 => "Postcode",
    4 => "ContactForename",
    5 => "ContactSurname",
    6 => "ContactPhone",
    7 => "ContactEmail",
    8 => "ContactFax",
    9 => "Description",
    10 => "Website",
    11 => "ParkingSpaces", /* ??????????? */
    12 => "CyclePark",
    13 => "DisabledFacilitiesNotes"
);


$template_providers = Array
(
    0 => "Organisation ID",
    1 => "Name",
    2 => "ProjectDepartment",
    3 => "BuildingNameNo",
    4 => "Street",
    5 => "Town",
    6 => "PostTown",
    7 => "County",
    8 => "PostCode",
    9 => "event contact name",
    10 => "ContactSurname",
    11 => "event contact email",
    12 => "event contact telephone*",
    13 => "ContactFax",
    14 => "description",
    15 => "Age range*",
    16 => "MaxAge",
    17 => "website"
);
$xmleq_providers = Array
(
    0 => "DPProviderID",
    1 => "Name",
    2 => "ProjectDepartment",
    3 => "BuildingNameNo",
    4 => "Street",
    5 => "Town",
    6 => "PostTown",
    7 => "County",
    8 => "Postcode",
    9 => "ContactForename",
    10 => "ContactSurname",
    11 => "ContactEmail",
    12 => "ContactPhone",
    13 => "ContactFax",
    14 => "Description",
    15 => "MinAge",
    16 => "MaxAge",
    17 => "Website"
);

//$daytext = Array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday");
//$daytext = Array("sunday"=>0, "monday"=>1, "tuesday"=>2, "wednesday"=>3, "thursday"=>4, "friday"=>5, "saturday"=>6);
function daytext($day) {
    $day = strtolower(rtrim(trim($day),"s"));
    switch ($day) {
        case "sunday":
            return 0; break;
        case "monday":
            return 1; break;
        case "tuesday":
            return 2; break;
        case "wednesday":
            return 3; break;
        case "thursday":
            return 4; break;
        case "friday":
            return 5; break;
        case "saturday":
            return 6; break;
        case "sun":
            return 0; break;
        case "mon":
            return 1; break;
        case "tue":
            return 2; break;
        case "wed":
            return 3; break;
        case "thur":
            return 4; break;
        case "thu":
            return 4; break;
        case "fri":
            return 5; break;
        case "sat":
            return 6; break;
        default:
            return FALSE; break;
    }
}


function checkhead($header, $template) {
/*    foreach ($header as $key => $field) {
        if (chop($field) != $template[$key]) die("ERROR: Incorrect order of column, $key should be ".$template[$key]." not $field.");
    }*/
}

function btoa($bdate, $vvv=true) {
    if (ereg('^([0-9]+)/([0-9]+)/([0-9]+)',$bdate,$arr)) {
        return $arr[2]."/".$arr[1]."/".$arr[3];
    }
    elseif (ereg('^([0-9]+).([0-9]+).([0-9]+)',$bdate,$arr)) {
        return $arr[2]."/".$arr[1]."/".$arr[3];
    }
    else {
        if ($vvv) {
            if (strlen($bdate) == 0) berror("No date value supplied.");
            else berror("Incorrectly formatted date! $bdate");
        }
        return FALSE;
    }
}

function children_from_array($dom, $pnode, $array) {
    if ($array == "") {
        berror("The requested venue/provider does not exist!");
    } else {
    foreach ($array as $field => $text) {
        if ($text != "") {
            #echo $field." ".$text."<br/>";
            $tmp = $dom->createElement($field);
            $pnode->appendChild($tmp);
            $tmp->appendChild($dom->createTextNode(trim($text, "\x7f..\xff\x0..\x1f\t\r\n\0 ")));        
        }
    }
    }
    return $pnode;
}

function children_from_array2($dom, $pnode, $field, $array) {
    foreach ($array as $text) {
        if ($text != "") {
            // HACK FOR HALTON CATEGORIES
            if (strtolower(trim($text)) == "learning") $text = "Learning and Life Skills"; 
            #echo $field."<br/>";
            $tmp = $dom->createElement($field);
            $pnode->appendChild($tmp);
            $tmp->appendChild($dom->createTextNode(trimbadchar($text)));        
        }
    }
    return $pnode;
}

function trimbadchar($text) {
    return trim($text, "\x7f..\xff\x0..\x1f\t\r\n\0 ");
}

function make_date($date, $time, $extra="") {
    global $fix;
    if (!($adate = btoa($date))) return FALSE; 
    else {
        if ($time == "") {
            berror("Time not supplied!");
        }
        $d = $adate." ".$time;
        if (!$newdate = strtotime($d)) {
            berror("Invalid date: $d");
            return FALSE;
        }
        #echo date("c", $newdate)." ".$newdate." ".time()."<br/>";
        if ($newdate < time()/*mktime(0,0,0,2,1,2010)*/ &&  $fix) {
            berror("<del>Date and time is in the past. $d</del> <i>Activity dropped</i>");
            return FALSE;
        }
        /*elseif ($newdate > mktime(0,0,0,2,15,2010) &&  $fix) {
            berror("<del>Date and time is past specified limit. $d</del> <i>Activity dropped</i>");
            return FALSE;
        }*/
        else {
            if ($newdate < time()/*mktime(0,0,0,1,1,2010)*/) berror("Date and time is in the past. $d");
            if ($extra == "+1") $newdate += 24*60*60;
            return date("c", $newdate);
        }
    }
}

function berror($text, $id=false) {
    global $debug, $row, $verb;
    if (!$id) $id = $row[0];
    if ($debug) {
        if (strpos($text,"<del>") === FALSE || $verb == 1)
            echo "<b>ID ".$id."</b> ".$text."<br/>";
    }
}

function check_format($var) {

}

function append_extra($id) {
    global $locala;
    if ($locala == "halton-help") $id = "H".$id;
    elseif ($locala == "knowsley") $id = trim(str_ireplace("ven", "Ven", $id));
    return $id;
}

function clever_split($text,$spaces=true) {
    if (strpos($text, ";") !== FALSE)
        return split(";", $text);
    elseif (strpos($text, ",") !== FALSE)
        return split(",", $text);
    else {
        if ($spaces)
            return split(" ", $text);
        else
            return array($text);
    }
}

function isHoliday($curr) {
    #if ($curr > mktime(0, 0, 0, 12, 24, 2009) && $curr < mktime(0,0,0,1,4,2010)) return true;
    #if ($curr < mktime(0, 0, 0, 4, 26, 2010)) return true;
    if ($curr > mktime(0, 0, 0, 5, 31, 2010) && $curr < mktime(0, 0, 0, 6, 5, 2010))  return true;
    else return false;
}

?>
