<?php
# Copyright (c) 2009 Ben Webb <bjwebb67@googlemail.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.

error_reporting(E_ALL  & ~E_NOTICE & ~E_DEPRECATED);
date_default_timezone_set('UTC'); 

if ($_GET['la']) $locala = $_GET['la'];
else $locala = "example";
$badv = $_GET['badv'];
$debug = true;
$fix = true;
$verb = $_GET['v'];

require_once "parse_defs.php";
require_once "parse_venues.php";

$spread = fopen("data/".$locala."_a1.csv", "r");

$header = fgetcsv($spread);
#print_r($header);
checkhead($header,$template);

$doc = new DOMDocument();
$doc->formatOutput = true;

$tl = $doc->createElement("Activities");
$doc->appendChild($tl);

if (!$badv) {
    echo "<h2>Parsing venues...</h2>";
    $venues = venues();
}
else $venues = array();
echo "<h2>Parsing organisations...</h2>";
$providers = providers();
echo "<h2>Parsing one-off activities...</h2>";

function parse_activities($spread, $xmleq2, $recur=false) {
    global $doc, $tl, $venues, $providers, $row, $fix, $verb, $debug, $locala, $daytext, $badv; 
    #print_r($xmleq2);
    $i = 0;
    $badi = 0;
    $id_ext = Array();
    while ($row = fgetcsv($spread)) {
        #print_r($row); echo "<br/>";
        if ($row[0] != "") {
        
        $s = $xmleq2[3][0];
        $e = $xmleq2[4][0];
        #echo $s." ".$e."OOO";
        
        $multiday=false;
        $days = Array(0,0,0,0,0,0,0);
        if ($recur) {
            $row[0] = "R".$row[0];
            $row[6] = strtolower(trim($row[6]));
            if ($row[6] == "weekly")
                $increment = 7;
            elseif ($row[6] == "fortnightly")
                $increment = 14;
            elseif ($row[6] == "monthly")
                $increment = 28;
            elseif ($row[6] == "daily")
                $increment = 1;
            elseif ($row[6] == "one off")
                $increment = 100;
            elseif ($row[6] == "" && $locala == "halton") {
                if ($fix) {
                    berror("<del>Frequency not given.</del> <i>Presuming daily</i>");
                    $increment = 1;
                }
                else
                    berror("Frequency not given.");
            }
            elseif ($row[6] == "" && daytext($row[5]) && $fix) {
                berror("<del>Frequency not given.</del> <i>Presuming weekly</i>");
                $increment = 7;
            } 
            else {
                berror("Frequency \"".$row[6]."\" is not recognised.");
                $increment = -1;
            }
            if ($adate = btoa($row[3],false))
                $from = strtotime($adate);
            else
                $from = mktime(0, 0, 0, 7, 4, 2009);
            if ($adate = btoa($row[4],false))
                $to = strtotime($adate);
            else
                #$to = mktime(0, 0, 0, 6, 30, 2010);
                $to = mktime(0, 0, 0, 8, 31, 2010);
            if (($increment == 7) && daytext($row[5]) === FALSE) {
                              # | $increment == 1
                if (strpos($row[5],",")) {
                    $ds = explode(",",$row[5]);
                    foreach ($ds as $d) {
                        $days[daytext($d)] = 1;
                    }
                }
                elseif (strpos($row[5],";")) {
                    $ds = explode(";",$row[5]);
                    foreach ($ds as $d) {
                        $days[daytext($d)] = 1;
                    }
                }
                elseif (ereg('^([a-zA-z ]+)\-([a-zA-z ]+)',$row[5],$arr)) {
                    #echo trim($arr[1]).",".trim($arr[2]);
                    $a = daytext(trim($arr[1]));
                    $b = daytext(trim($arr[2]));
                    if ($a === FALSE) berror("Bad day ".$arr[1]);
                    elseif ($b === FALSE) berror("Bad day ".$arr[2]);
                    else {
                        if ($a < $b) {
                            for ($i=$a; $i <= $b; $i++) {
                                $days[$i] = 1;
                            }
                        }
                        else {
                            $days = Array(1,1,1,1,1,1,1);
                            for ($i=$b+1; $i < $a; $i++) {
                                $days[$i] = 0;
                            }
                        }
                    }
                }
                elseif (ereg('^([a-zA-z ]+)\&([a-zA-z ]+)',$row[5],$arr)) {
                    #echo trim($arr[1]).".".trim($arr[2]);
                    $a = daytext(trim($arr[1]));
                    $b = daytext(trim($arr[2]));
                    if ($a === FALSE) berror("Bad day ".$arr[1]);
                    elseif ($b === FALSE) berror("Bad day ".$arr[2]);
                    $days[$a] = 1;
                    $days[$b] = 1;
                }
                else {
                    berror("Unrecognised day ".$row[5]);
                }
                $increment = 1;
                $multiday = true;
                #print_r($days);
            }
            if ($increment == 7 || $increment == 14) {
                if (daytext($row[5]) === FALSE)
                    berror("Unrecognised day ".$row[5]);
                $diff = daytext($row[5]) - date("w", $from);
                if ($diff < 0) $diff += 7;
                $curr = $from + $diff*24*60*60;
            }
            else
                $curr = $from;
            $curr += 61*60; // VERY MESSY DAYLIGHT SAVINGS HACK
        }
        
        $row[0] = append_extra($row[0]);
        
        $origid = $row[0];
        
        $j = 0;
        $sss = $row[$s];
        $eee = $row[$e];
        
        $link = false;
        
        do {
        #echo $increment;
        if (!$recur || $increment > 0) {
        if ((!$multiday || ($multiday && $days[date("w", $curr)])) && (!$_REQUEST['hol'] || !isHoliday($curr))) {
        
        $extra = "";
        if ($recur) { //TODO cleanup this
            $row[3] = date("d/m/Y",$curr);
            #echo $row[0];
            #echo $curr;
            #echo $row[3];
            if (($sss == "" || $sss === "varies" || $sss == "Flexible times") &&
                ($eee == "" || $eee === "varies" || $eee == "Flexible times")) {
                #echo "Test";
                $row[4] = date("d/m/Y",$curr+24*60*60);
                $sss = "00:00"; $eee = "00:00";
            }
            else
                $row[4] = date("d/m/Y",$curr);
        }
        else {
            if (($sss == "" || $sss === "Call for details" || $sss == "TBC") &&
                ($eee == "" || $eee === "Call for details" || $eee == "TBC")) {
                if (ereg('^([0-9]+)[/\.]([0-9]+)[/\.]([0-9]+)',$row[4],$arr)) {
                    $extra =  "+1";
                    $sss = "00:00"; $eee = "00:00";
                }
            }
        }
        
        if (!$id_ext[$origid]) {
            $id_ext[$origid] = 1;
        }
        else {
            #$link = $origid;
            #$link = $origid."-".($id_ext[$origid]-1);
            $row[0] = $origid."-".$id_ext[$origid];
            $id_ext[$origid]++;
        }
        
        $row[$s] = make_date($row[3],$sss);
        $row[$e] = make_date($row[4],$eee,$extra);
        
        if ($row[$s] == FALSE || $row[$e] == FALSE) { }
        else {
        
        $activity[$i][$j] = $doc->createElement("Activity");
        $tl->appendChild($activity[$i][$j]);
        
        if ($row[$xmleq2[8][0]] == "" && $row[$xmleq2[9][0]] == "") {
            if ($fix) {
                $row[$xmleq2[8][0]] = "n/a";
                berror("<del>Either a contact email or a phone number must be specified</del> <i>Replaced with placeholder text.</i>");
            }
        }
        
        foreach ($xmleq2 as $xmlrow) {
            $key = $xmlrow[0];
            if ($row[$key] == "" && $xmlrow[2]) {
                if (($key == $xmleq2[7][0] || $key == 2) && $fix) {
                    if ($key == $xmleq2[7][0])
                        $row[$key] = "General enquiries";
                    elseif ($key == 2)
                        $row[$key] = "No description available.";
                    berror("<del>Required field ".$xmlrow[1]." is missing.</del> <i>Replaced with placeholder text</i>");
                }
                else {
                    berror("Required field ".$xmlrow[1]." is missing.");
                }
            }
            if ($row[$key] != "") {
                $info[$key] = $doc->createElement($xmlrow[1]);
                $activity[$i][$j]->appendChild($info[$key]);
                if ($key == 1 && $locala == "knowsley") $row[$key] = ucwords(strtolower($row[$key]));
                if ($key < $xmleq2[14][0])
                    $info[$key]->appendChild($doc->createTextNode(preg_replace('/[\x7f-\xff\x0-\x1f\t\r\n\0]/',"",$row[$key])));
            }
        }
        
        if ($link) {
                $linknode = $doc->createElement("LinkWithActivitySourceID");
                $activity[$i][$j]->appendChild($linknode);
                $linknode->appendChild($doc->createTextNode($link));
        }
        
        if ($row[9] == "" && $row[10] == "") {
            /*if ($fix) {
            
            }
            else */
            berror("Either a contact email or a contact address must be specified");
        }
         
        // Categories
        $num_c = $xmleq2[14][0];
        if ($row[$num_c] != "") {
            $info[$num_c] = children_from_array2($doc, $info[$num_c], "Category", clever_split($row[$num_c],false));
        }        
           
        // Keywords
        $num_k = $xmleq2[15][0];
        if ($row[$num_k] != "") {
            $info[$num_k] = children_from_array2($doc, $info[$num_k], "Keyword", clever_split($row[$num_k]));
        }
        
        // ECMCodes
        $num_e = $xmleq2[16][0];
        if ($row[$num_e] != "") {
            $info[$num_e] = children_from_array2($doc, $info[$num_e], "ECMCode", split(";",$row[$num_e]));
        }
        
        //Venue
        $num_v = $xmleq2[5][0];
        if ($badv) {
            $vs = explode(",", $row[$num_v]);
            $badid = append_extra($row[$num_v]);
            if (!$venues[$badid]) {
                #echo $badi;
                $venues[$badid] = array(
                    "ProviderVenueID" => $badid,
                    "Name" => $vs[0],
                    "BuildingNameNo" => $vs[0],
                    "Postcode" => $vs[count($vs)-1],
                    "ContactForename" => "General",
                    "ContactSurname" => "enquiries",
                    "ContactPhone" => "n/a"
                );
                #$badi++;
            }
        }
        $info[$num_v] = children_from_array($doc, $info[$num_v], $venues[append_extra($row[$num_v])]);
        $venues[append_extra($row[$num_v])] = Array("ProviderVenueID" => $venues[append_extra($row[$num_v])]["ProviderVenueID"]);
        #print_r($venues);
        
        //Activity Provider
        $num_ap = $xmleq2[6][0];
        if ($row[$num_ap]) {
            $info[$num_ap] = children_from_array($doc, $info[$num_ap], $providers[append_extra($row[$num_ap])]);
            $providers[append_extra($row[$num_ap])] = Array("DPProviderID" => $providers[append_extra($row[$num_ap])]["DPProviderID"]);
        }
        
        $i++;
        if (!$link) $link = $row[0];
        }
        $j++;
        }
        }
        else break;
        #else echo date("w",$curr);
        $curr += $increment*24*60*60;
        } while ($recur && $curr <= $to);
        }
    }
    #berror("Number of results: ".$i);
}
parse_activities($spread,$xmleq2);

echo "<h2>Parsing recurring activities...</h2>";
/*if ($locala == "islington") {*/
    $spread_r = fopen("data/".$locala."_ar.csv", "r");
    $header_r = fgetcsv($spread_r);
    parse_activities($spread_r,$xmleq2_recurring,true);
/*}*/

$doc->save("out/$locala.xml");
echo "<a href=\"out/$locala.xml\">View XML</a>";
?>
